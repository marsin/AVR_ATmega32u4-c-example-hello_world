
#ifndef F_CPU
  #define F_CPU 16000000ul
#endif

#include <avr/io.h>
#include <util/delay.h>

#if TEST == 1
  #define LED_PORT PORTB
  #define LED_DDR  DDRB
  #define LED_POS  0     // Onboard RX LED (orange)
#elif TEST == 2
  #define LED_PORT PORTD
  #define LED_DDR  DDRD
  #define LED_POS  7     // External LED (on Arduino Digital Pin 6)
  #define KEY_PORT PORTD
  #define KEY_DDR  DDRD
  #define KEY_PIN  PIND
  #define KEY_POS  4     // External key (on Arduino Digital Pin 4)
#else
  #error Undefined TEST number!
#endif


#if TEST == 1
/** TEST 1: Blinking orange onboard RX LED (no external circuit required). */
int main(void)
{
	LED_DDR  |=  (1<<LED_POS); // output
	LED_PORT &= ~(1<<LED_POS); // LED off

	while (!0) {
		LED_PORT |= (1<<LED_POS);
		_delay_ms(500);
		LED_PORT &= ~(1<<LED_POS);
		_delay_ms(500);
	}

	return 0;
}


#elif TEST == 2 // TEST
/** TEST 2: External LED blinks when external KEY is pressed. */
int main(void)
{
	LED_DDR  |=  (1<<LED_POS); // output
	LED_PORT &= ~(1<<LED_POS); // LED off

	KEY_DDR  &= ~(1<<KEY_POS); // input
	KEY_PORT |=  (1<<KEY_POS); // Pull-Up resistor

	while (!0) {
		if (KEY_PIN & (1<<KEY_POS)) {
//			LED_PORT |= (1<<LED_POS);
			LED_PORT &= ~(1<<LED_POS);
		} else {
			LED_PORT &= ~(1<<LED_POS);
			_delay_ms(50);
			LED_PORT |= (1<<LED_POS);
			_delay_ms(50);
		}
	}

	return 0;
}
#endif // TEST

